<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductosController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('productos/index.html.twig', [
            'controller_name' => 'ProductosController',
        ]);
    }

    /**
     * @Route("/producto/{id}", name="producto")
     */
    public function producto($id)
    {
        return $this->render('productos/producto.html.twig', [
            'controller_name' => 'Quiero mostrar un único producto'.$id,
        ]);
    }
}
